import 'package:egg_timer/model/egg_timer.dart';
import 'package:flutter/material.dart';

import 'flatten_button.dart';

class EggtimerControl extends StatelessWidget {
  final TimerState state;
  final VoidCallback onPause;
  final VoidCallback onResume;
  final VoidCallback onRestart;
  final VoidCallback onReset;

  const EggtimerControl(
      {Key key,
      this.state,
      this.onPause,
      this.onResume,
      this.onRestart,
      this.onReset})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new EggBtn(
              icon: Icons.refresh,
              title: '重新开始',
              onPressed: onRestart,
            ),
            EggBtn(
              icon: Icons.arrow_back,
              title: '重置',
              onPressed: onReset,
            ),
          ],
        ),
        state == TimerState.running
            ? EggBtn(
                icon: Icons.pause,
                title: '暂停',
                onPressed: onPause,
              )
            : state == TimerState.pause
                ? EggBtn(
                    icon: Icons.play_arrow,
                    title: '继续',
                    onPressed: onResume,
                  )
                : EggBtn(
                    icon: Icons.info,
                    title: '开始(请拖动指针)',
                    onPressed: null,
                  ),
      ],
    );
  }
}
