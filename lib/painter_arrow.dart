import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ArrowPainter extends CustomPainter {
  final Paint dialArrowPaint;
  final double rotation;

  ArrowPainter({this.rotation: 0}) : dialArrowPaint = Paint() {
    dialArrowPaint.color = Colors.red;
    dialArrowPaint.style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.save(); //初始位置
    final radius = size.height / 2;
    canvas.translate(radius, radius); //以中间开始
    canvas.rotate(rotation);

    Path path = Path();
    path.moveTo(0, -radius - 10); //上移
    path.lineTo(10, -radius + 5);
    path.lineTo(-10, -radius + 5);
    path.close(); // 关闭曲线

    canvas.drawPath(path, dialArrowPaint);
    canvas.drawShadow(path, Colors.black, 3.0, false);
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
