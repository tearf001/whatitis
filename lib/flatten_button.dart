import 'package:flutter/material.dart';

class EggBtn extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function onPressed;

  const EggBtn({Key key, @required this.icon, @required this.title, this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FlatButton(
        onPressed: onPressed,
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                color: Colors.black,
              ),
              Text(title)
            ],
          ),
        ));
  }
}
