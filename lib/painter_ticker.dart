import 'dart:math';
import 'package:egg_timer/_const.dart';
import 'package:flutter/material.dart';

class TickerPainter extends CustomPainter {
  final tickCount;
  final tickPaint;
  final ticksInset;
  final ticksPersection;
  final radicalPad;

  final TextPainter textPainter;
  final textStyle;

  TickerPainter(
      {this.tickCount: 60,
      this.ticksPersection: 5,
      this.radicalPad: 4,
      this.ticksInset: 0.0})
      : tickPaint = Paint()
          ..color = Colors.black
          ..strokeWidth = 1.5,
        textPainter = TextPainter(
          textAlign: TextAlign.center,
          textDirection: TextDirection.ltr,
        ),
        textStyle = const TextStyle(color: Colors.black, fontSize: 20);

  @override
  void paint(Canvas canvas, Size size) {
    final radius = size.width / 2;
    canvas.translate(radius, radius);
    canvas.save();
    for (var i = 0; i < tickCount; i++) {
      bool isClock = i % ticksPersection == 0;
      final tickHere = isClock ? const_ticker_long : const_ticker_short;
      canvas.drawLine(Offset(0, -radius - radicalPad),
          Offset(0, -radius - radicalPad - tickHere), tickPaint);

      if (isClock) {
        //paint text
        canvas.save();
        canvas.translate(0, -radius - 30);
        textPainter.text = TextSpan(text: '$i', style: textStyle);
        // layout the text
        textPainter.layout();
        ///TODO 理解绘画.当layout时,宽度就确认了
        textPainter.paint(canvas, Offset(-textPainter.width/2, -textPainter.height/2));
        canvas.restore();
      }
      canvas.rotate(2 * pi / tickCount);
    }
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
