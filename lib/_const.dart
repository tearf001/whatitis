import 'package:flutter/rendering.dart';

const gradient_top_color = const Color(0xfff5f5f5);
const gradient_bottom_color = const Color(0xffe8e8e8);
const const_ticker_long = 10.0;
const const_ticker_short = 3.0;
