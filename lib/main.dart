import 'package:egg_timer/egg_timer_dial.dart';
import 'package:egg_timer/model/egg_timer.dart';
import 'package:egg_timer/time_control.dart';
import 'package:egg_timer/time_display.dart';
import 'package:flutter/material.dart';

import '_const.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  gradient_top_color,
                  gradient_bottom_color,
                ]),
          ),
          child: Column(children: [
            TimeDisplay(),
            EggTimeDial(),
            Expanded(
              child: Container(),
            ),
            StreamBuilder<TimerState>(
                stream: EggTimer.inst.state,
                builder: (context, snapshot) {
                  return EggtimerControl(
                    state:
                        snapshot.hasData ? snapshot.data : TimerState.ready,
                    onPause: () => EggTimer.inst.emitEvent(TimerEvent.pause),
                    onReset: () => EggTimer.inst.emitEvent(TimerEvent.reset),
                    onRestart: () =>
                        EggTimer.inst.emitEvent(TimerEvent.restart),
                    onResume: () => EggTimer.inst.emitEvent(TimerEvent.start),
                  );
                }),
          ]),
        ),
      ),
    );
  }
}
