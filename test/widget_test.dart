// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:egg_timer/main.dart';

void main() {
  final TestWidgetsFlutterBinding binding =
      TestWidgetsFlutterBinding.ensureInitialized();
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {

    await binding.setSurfaceSize(Size(1080, 2192));

    await tester.pumpWidget(HomePage());

    expect(find.text('00'), findsOneWidget);
    expect(find.text('暂停'), findsNothing);

    await tester.dragFrom(Offset(540,540), Offset(600,600));
    
    await tester.pump();

    expect(find.text('00'), findsNothing);
    expect(find.text('暂停'), findsOneWidget);
  });
}
