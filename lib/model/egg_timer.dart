import 'dart:async';
import 'package:rxdart/rxdart.dart';

enum TimerState { ready, running, pause }
enum TimerEvent { start, restart, pause, reset ,doneTiming}

class EggTimer {
  static EggTimer _inst;
  static EggTimer get inst => _inst ??= EggTimer();
  BehaviorSubject _ticking = BehaviorSubject(); //计时器流
  BehaviorSubject<TimerState> _state = BehaviorSubject<TimerState>(); //状态流
  BehaviorSubject<TimerEvent> _event = BehaviorSubject<TimerEvent>(); //事件流(可以管理状态)
  BehaviorSubject<int> _dialDeltaSeconds = BehaviorSubject<int>(); //拨动流

  Stream get ticking => _ticking.stream;
  Duration startTime = Duration(seconds: 0);
  Stream<int> get dialing => _dialDeltaSeconds.stream;

  Stream<TimerState> get state => _state.stream;

  void Function() get tick => () =>_ticking.add(1); //tick the timer
  void Function(TimerState) get updateState => _state.add; // update state
  void Function(TimerEvent) get emitEvent => _event.add;  // emit event
  void Function(int) get dial => _dialDeltaSeconds.add; //拨动表盘

  Duration get currentTime =>
      Duration(seconds: startTime.inSeconds - stopwatch.elapsed.inSeconds);

  intergerStartTime() { //规整起始时间
    startTime = Duration(minutes: startTime.inMinutes);
  }

  Timer timer;
  Stopwatch stopwatch = Stopwatch();

  Timer t;
  runPeriodic() {
    t = Timer.periodic(Duration(seconds: 1), (timer) {
      if(!stopwatch.isRunning)
        timer.cancel();
      print('当前时间$currentTime  / 开始时间 $startTime');
      var current = currentTime;
      if(current.inSeconds == 0)
        emitEvent(TimerEvent.doneTiming);
      tick();
    });
  }

  EggTimer() {
    _dialDeltaSeconds.listen((p) {
      startTime += Duration(seconds: p);
    });
    _event.listen((event) {
      switch (event) {
        case TimerEvent.start:
          updateState(TimerState.running);
          stopwatch.start();
          break;
        case TimerEvent.restart:
          if(startTime!=Duration.zero){ //只有开始时间没清空,才可以重新开始
            updateState(TimerState.running);
            stopwatch.reset();
            stopwatch.start();
          }
          break;
        case TimerEvent.pause:
          updateState(TimerState.pause);
          stopwatch.stop();
          break;
        case TimerEvent.reset: //全部归零,包括开始时间
          updateState(TimerState.ready);
          startTime = Duration(seconds: 0);
          dial(0);//调指针 没有变化,但是触发->表针渲染器重绘
          stopwatch.stop();
          stopwatch.reset();
          break;
        case TimerEvent.doneTiming:
          updateState(TimerState.ready);
          dial(0);//调指针 没有变化,但是触发->表针渲染器重绘
          stopwatch.stop();
          break;
      }
      if (stopwatch.isRunning) {
        runPeriodic();
      } else {
        t?.cancel();
      }
    });
  }
  void dispose() {
    _ticking.close();
    _state.close();
    _event.close();
    _dialDeltaSeconds.close();
    timer?.cancel();
  }

  // // get currentTime => _currentTime;
  // // set currentTime(newTime) {
  // //   if (state == TimerState.ready) {
  // //     _currentTime = newTime;
  // //     //_lastTime = _currentTime;
  // //   }
  // // }

  // void resume() {
  //   // state = TimerState.running;
  //   // stopwatch.start();
  //   // _tick();
  //   updateState(TimerState.ready);
  // }

  // void pause() {
  //   // state = TimerState.pause;
  //   // stopwatch.stop();
  //   updateState(TimerState.pause);
  // }

  // void _tick() {
  //   print('current : $currentTime');
  //   _currentTime = _lastTime - Duration(seconds: stopwatch.elapsed.inSeconds);
  //   if (_currentTime.inSeconds > 0) {
  //     //还有时间
  //     if (state == TimerState.running)
  //       timer = Timer(const Duration(seconds: 1), _tick);
  //   } else
  //     state = TimerState.ready;
  //   onUpdate?.call();
  // }

  // void reset() {
  //   print('reset');
  //   state = TimerState.ready;
  //   currentTime = Duration(seconds: 0);
  //   timer?.cancel();
  //   stopwatch.stop();
  //   stopwatch.reset();
  //   onUpdate?.call();
  // }

  // void restart() {
  //   print('restart');
  //   currentTime = _lastTime;
  //   resume();
  // }
}
