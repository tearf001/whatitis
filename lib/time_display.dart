import 'package:egg_timer/model/egg_timer.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const TextStyle textStyle = const TextStyle(
  color: Colors.black,
  fontFamily: 'Consolas',
  fontSize: 100,
  fontWeight: FontWeight.bold,
  letterSpacing: 3,
);

String format(Duration duration, [fmt = 'mm:ss']) {
  return DateFormat(fmt).format(DateTime(0, 0, 0, 0, 0, duration.inSeconds));
}

class TimeDisplay extends StatefulWidget {
  @override
  _TimeDisplayState createState() => _TimeDisplayState();
}

class _TimeDisplayState extends State<TimeDisplay>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> startTimeTran;
  Animation<double> countDownTran;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    startTimeTran = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(parent: animationController, curve: Curves.ease));
    countDownTran = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(parent: animationController, curve: Curves.ease));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<TimerState>(
        stream: EggTimer.inst.state,
        builder: (context, stateSnap) {
          if (stateSnap.hasData) {
            if (stateSnap.data == TimerState.running) {
              animationController.forward(); //执行动画
            } else if (stateSnap.data == TimerState.ready) //运行,或者别的什么状态
              animationController.reverse(); //还原
          }
          return Padding(
            padding: const EdgeInsets.all(50.0),
            child: AnimatedBuilder(
                animation: animationController,
                builder: (_, w) {
                  return Stack(
                    alignment: Alignment.center,
                    children: [
                      Transform(
                        transform: Matrix4.translationValues(
                            0, -200 * startTimeTran.value, 0),
                        child: StreamBuilder<int>(
                            stream: EggTimer.inst.dialing,
                            builder: (context, dialSnap) {
                              return Text(
                                dialSnap.hasData && dialSnap.data!=0
                                    ? '${format(EggTimer.inst.startTime, 'mm')}'
                                    : '00',
                                style: textStyle,
                              );
                            }),
                      ),
                      Opacity(
                        opacity: countDownTran.value,
                        child: StreamBuilder(
                            stream: EggTimer.inst.ticking,
                            builder: (context, snapshot) {
                              return Text(
                                '${format(snapshot.hasData ? EggTimer.inst.currentTime : EggTimer.inst.startTime)}',
                                style: textStyle,
                              );
                            }),
                      )
                    ],
                  );
                }),
          );
        });
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
