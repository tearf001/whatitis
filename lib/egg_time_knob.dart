import 'dart:math';

import 'package:flutter/material.dart';

import '_const.dart';
import 'painter_arrow.dart';

class EggTimeKnob extends StatefulWidget {
  final double rotation;
  const EggTimeKnob({Key key, this.rotation}) : super(key: key);
  @override
  _EggTimeKnobState createState() => _EggTimeKnobState();
}

class _EggTimeKnobState extends State<EggTimeKnob> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: ArrowPainter(rotation: widget.rotation),
          ),
        ),
        Container(
          //第2个圆
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.blue,
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    gradient_top_color,
                    gradient_bottom_color,
                  ]),
              boxShadow: [
                BoxShadow(
                    color: Color(0x44000000),
                    blurRadius: 2,
                    spreadRadius: 1,
                    offset: Offset(0, 1.0))
              ]),
          child: Container(
            //第3个圆
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1.5, color: const Color(0xffdfdfdf)),
            ),
            child: Center(
              child: Transform.rotate(
                angle: widget.rotation,
                child: 
                // Image.network(
                //   'https://avatars3.githubusercontent.com/u/14101776',
                Image.asset(
                  'assets/flutter_logo.png',
                  width: 50,
                  height: 50,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
